﻿using PEJL_WPF_Examples;
using Q.Class;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Threading;
using System.Runtime.InteropServices;
//https git support test

namespace Q
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region private members
        //全局激活Q的快捷键
        public HotKey globalHK;
        ////全局激活tc的快捷键
        //private HotKey tcHK;
        ////全局激活onenote的快捷键
        //private HotKey onenoteHK;
        ////全局激活vs的快捷键
        //private HotKey vs;
        //private HotKey qq;
        //private int currentBrowser;
        private int currentSearch;
        //private List<string> browsers = new List<string>();
        private List<string> searchTypes = new List<string>();
        private CloseActiveProcess cap = new CloseActiveProcess();
        private Stopwatch sw = new Stopwatch();
        private InterceptKeys ik;
        //bool afterPaste = false; 
        #endregion


        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        string GetActiveProcessFileName()
        {
            IntPtr hwnd = GetForegroundWindow();
            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            Process p = Process.GetProcessById((int)pid);
            return p.MainModule.FileName;
        }


        public MainWindow()
        {
            InitializeComponent();

            try
            {
                Loaded += MainWindowLoaded;
                KeyDown += MainWindowKeyDown;
                PreviewKeyDown += (s, e) =>
                {
                    if (tb.IsFocused == false)
                        tb.Focus();
                };
                MinimizeToTray.Enable(this);
                //
                //browsers.AddRange(new string[] { "iexplore.exe", "chrome.exe", @"C:\Users\Hongzhi\AppData\Local\360Chrome\Chrome\Application\360chrome.exe" });
                searchTypes.AddRange(new string[] { "https://www.google.com/search?q=", "http://dict.youdao.com/search?q=", "http://en.wikipedia.org/w/index.php?search=" });
                DataObject.AddPastingHandler(tb, onpaste);
                //tab key as enter
                tb.PreviewKeyDown += (s, e) =>
                {
                    if (e.Key == Key.Tab)
                    {
                        Execute(currentSearch, tb.Text);
                        e.Handled = true;
                        this.WindowState = System.Windows.WindowState.Minimized;
                    }
                };
                Deactivated += (s, e) =>
                {
                    this.WindowState = WindowState.Minimized;
                };
            }
            catch (Exception)
            {

            }



        }
        private void MainWindowLoaded(object sender, RoutedEventArgs e)
        {
            //Stack<InterceptKeyEventArgs> pressedKey = new Stack<InterceptKeyEventArgs>();
            bool firstWinKeyDownReceived = false;


            //Debug.Write("loaded");
            ik = InterceptKeys.Current;
            var lowKeyStream = ik.GetKeyStream().Publish();
            //ObserveOn(new SynchronizationContextScheduler(SynchronizationContext.Current))
            lowKeyStream
                .Subscribe(async ee =>
                {
                    //pressedKey.Push(ee);
                    //Debug.Write(ee.Key.ToString() + " " + ee.KeyDirection.ToString());
                    if (ee.Key == System.Windows.Forms.Keys.LWin && ee.KeyDirection == KeyDirection.Down)
                    {
                        firstWinKeyDownReceived = true;
                        //pressedKey.Push(ee);
                    }
                    else
                    {
                        if (firstWinKeyDownReceived)
                        {
                            firstWinKeyDownReceived = false;
                            if (ee.Key == System.Windows.Forms.Keys.LWin && ee.KeyDirection == KeyDirection.Up)
                            {
                                Find.SwitchEng((IntPtr)0xffff);
                            }
                        }
                    }
                    if(ee.Key == System.Windows.Forms.Keys.Q && ee.KeyDirection == KeyDirection.Up)
                    {
                        var a = GetActiveProcessFileName();
                        if(a == "C:\\Program Files (x86)\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe")
                        {
                            //var a = new getSelectedText();
                            System.Windows.Forms.SendKeys.SendWait("^c");
                            await Task.Delay(250);
                            var text = Convert.ToString(System.Windows.Clipboard.GetText());
                            //MessageBox.Show(text);
                            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                            text = rgx.Replace(text, "");

                            //var text = Clipboard.GetText(TextDataFormat.Text);
                            if (!string.IsNullOrEmpty(text))
                            {
                                var apiResult = await LookUp(text) as Dictionary<string, object>;

                                var displayWindow = new LookupResult();
                                var displayString = "";

                                try
                                {
                                    displayString += concatArray(ToStringArray(apiResult["translation"]));
                                }
                                catch (Exception)
                                {

                                };
                                displayString += "\r\n\r\n";

                                try
                                {
                                    //displayString += apiResult.basic.explains.ToString();
                                    displayString += concatArray(ToStringArray((apiResult["basic"] as Dictionary<string, object>)["explains"]));
                                }
                                catch (Exception)
                                {

                                }
                                displayWindow.textBlock.Text = displayString;
                                displayWindow.Show();
                                displayWindow.Activate();
                                YouDao.pronounce(text);
                            }
                        }
                    }


                });
            lowKeyStream.Connect();

            //InputMethod.SetPreferredImeState(tb, InputMethodState.Off);
            Left = (SystemParameters.PrimaryScreenWidth - ActualWidth) / 2;
            Top = (SystemParameters.PrimaryScreenHeight - ActualHeight) / 3;
            WindowState = WindowState.Minimized;
            //var _currentBrowser = Settings.Default.CurrentBrowser;
            //currentBrowser = _currentBrowser;
            //switch (_currentBrowser)
            //{
            //case 1:
            //    r1.IsChecked = true;
            //    currentBrowser = 1;
            //    break;
            //case 2:
            //    r2.IsChecked = true;
            //    currentBrowser = 2;
            //    break;
            //case 3:
            //    r3.IsChecked = true;
            //    currentBrowser = 3;
            //    break;
            //default:
            //    break;
            //}
            var _currentSearch = 1;
            currentSearch = _currentSearch;
            switch (_currentSearch)
            {
            case 1:
                r4.IsChecked = true;
                currentSearch = 1;
                tb.SetValue(TextBoxHelper.WatermarkProperty, "Google Search");
                break;
            case 2:
                r5.IsChecked = true;
                tb.SetValue(TextBoxHelper.WatermarkProperty, "YouDao Search");
                currentSearch = 2;
                break;
            case 3:
                r6.IsChecked = true;
                currentSearch = 3;
                tb.SetValue(TextBoxHelper.WatermarkProperty, "Wikipedia Search");
                break;
            case 4:
                r7.IsChecked = true;
                currentSearch = 4;
                tb.SetValue(TextBoxHelper.WatermarkProperty, "有道 api");
                break;
            default:
                break;
            }


            AddHandler(System.Windows.Controls.Primitives.ToggleButton.CheckedEvent, new RoutedEventHandler(RadioChecked));



            globalHK = new HotKey(ModifierKeys.Control, System.Windows.Forms.Keys.Space, this);
            globalHK.HotKeyPressed += globalHKPressed;

            var tcHK = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.Q, this);
            tcHK.HotKeyPressed += async (s) =>
            {
                var result = Find.bringToFront("Total Commander (x64) 8.51a - Bmc Software Israel Ltd.");
                if (result == -1)
                {
                    var p = Process.Start(@"C:\Program Files\totalcmd\TOTALCMD64.EXE");
                    p.WaitForInputIdle();
                    ////Find.SwitchEng(p.MainWindowHandle);
                    //Find.bringToFront(p.MainWindowHandle);
                    await Task.Delay(700);
                    Find.bringToFront("Total Commander (x64) 8.51a - Bmc Software Israel Ltd.");
                }
            };

            var onenoteHK = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.R, this);
            onenoteHK.HotKeyPressed += async (s) =>
            {
                try
                {
                    Process[] ps = Process.GetProcessesByName("ONENOTE");
                    if (ps.Length > 0)
                    {
                        if (Find.GetPlacement(ps[0].MainWindowHandle).showCmd.ToString() != "Hide")
                        {
                            Find.FocusProcess(ps[0]);
                            Find.SwitchEng(ps[0].MainWindowHandle);
                        }
                        else
                        {
                            await Task.Delay(2000);
                            Process.Start(@"C:\Program Files (x86)\Microsoft Office\root\Office16\ONENOTE.EXE");
                        }
                    }
                    else
                    {
                        var p = Process.Start(@"C:\Program Files (x86)\Microsoft Office\root\Office16\ONENOTE.EXE");
                        //p.WaitForInputIdle();
                        //await Task.Delay(2000);
                        Find.SwitchEng((IntPtr)0xffff);
                    }
                }
                catch (Exception q)
                {
                    MessageBox.Show(q.Message);
                }
            };

            var qq = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.S, this);
            qq.HotKeyPressed += (s) => { Process.Start(@"C:\Program Files (x86)\Tencent\QQ\Bin\QQ.exe"); };

            var taskHK = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.D1, this);
            taskHK.HotKeyPressed += (s) => { Process.Start("taskmgr.exe", "/6"); };

            var cmder = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.D3, this);
            cmder.HotKeyPressed += (s) =>
            {
                Process.Start(@"c:\Users\crazy\OneDrive\Applications\cmder\Cmder.exe", @"/start c:\Users\crazy\Desktop\gits\");
            };

            var vs = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.D2, this);
            vs.HotKeyPressed += (s) =>
            {
                Process[] ps = Process.GetProcessesByName("devenv");
                if (ps.Length > 0)
                {
                    Find.FocusProcess(ps[0]);
                }
                else
                    Process.Start(@"devenv.exe");


            };

            var url = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.L, this);
            url.HotKeyPressed += (s) =>
            {
                //[a-zA-z]+://[^\s]*
                var text = Clipboard.GetText(TextDataFormat.Text);
                var match = Regex.Match(text, @"[a-zA-z]+://[^\s]*");
                if (match.Success)
                {
                    Process.Start(text);
                }
            };
            var lookupCopy = new HotKey(ModifierKeys.Shift, System.Windows.Forms.Keys.Enter, this);
            lookupCopy.HotKeyPressed += async (s) =>
            {
                //var a = new getSelectedText();
                var text = Convert.ToString(System.Windows.Clipboard.GetText());
                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                text = rgx.Replace(text, "");

                //var text = Clipboard.GetText(TextDataFormat.Text);
                if (!string.IsNullOrEmpty(text))
                {
                    var apiResult = await LookUp(text) as Dictionary<string, object>;

                    var displayWindow = new LookupResult();
                    var displayString = "";

                    try
                    {
                        displayString += concatArray(ToStringArray(apiResult["translation"]));
                    }
                    catch (Exception)
                    {

                    };
                    displayString += "\r\n\r\n";

                    try
                    {
                        //displayString += apiResult.basic.explains.ToString();
                        displayString += concatArray(ToStringArray((apiResult["basic"] as Dictionary<string, object>)["explains"]));
                    }
                    catch (Exception)
                    {

                    }
                    displayWindow.textBlock.Text = displayString;
                    displayWindow.Show();
                    displayWindow.Activate();
                    YouDao.pronounce(text);
                }
            };

            //var lookup = new HotKey(ModifierKeys.Control, System.Windows.Forms.Keys.Q, this);
            //lookup.HotKeyPressed += async (s) =>
            //{
            //    var a = new getSelectedText();
            //    var text = a.getSelectedTextHACK();
            //    Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            //    text = rgx.Replace(text, "");

            //    //var text = Clipboard.GetText(TextDataFormat.Text);
            //    if (!string.IsNullOrEmpty(text))
            //    {
            //        var apiResult = await LookUp(text) as Dictionary<string, object>;

            //        var displayWindow = new LookupResult();
            //        var displayString = "";

            //        try
            //        {
            //            displayString += concatArray(ToStringArray(apiResult["translation"]));
            //        }
            //        catch (Exception)
            //        {

            //        };
            //        displayString += "\r\n\r\n";

            //        try
            //        {
            //            //displayString += apiResult.basic.explains.ToString();
            //            displayString += concatArray(ToStringArray((apiResult["basic"] as Dictionary<string, object>)["explains"]));
            //        }
            //        catch (Exception)
            //        {

            //        }
            //        displayWindow.textBlock.Text = displayString;
            //        displayWindow.Show();
            //        displayWindow.Activate();
            //        YouDao.pronounce(text);
            //    }
            //    //[a-zA-z]+://[^\s]*
            //    //var text = Clipboard.GetText(TextDataFormat.Text);
            //    //var match = Regex.Match(text, @"[a-zA-z]+://[^\s]*");
            //    //if (match.Success)
            //    //{
            //    //    Process.Start(text);
            //    //}
            //};



            var webstorm = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.G, this);
            webstorm.HotKeyPressed += (s) =>
            {
                Process[] ps = Process.GetProcessesByName("WebStorm");
                if (ps.Length > 0)
                {
                    Find.FocusProcess(ps[0]);
                }
                else
                    Process.Start(@"C:\Program Files (x86)\JetBrains\WebStorm 8.0\bin\WebStorm.exe");



            };

            var notepad = new HotKey(ModifierKeys.Shift, System.Windows.Forms.Keys.Escape, this);
            notepad.HotKeyPressed += (s) =>
            {
                var p = Process.Start(@"c:\Users\crazy\OneDrive\Applications\Notepad++\notepad++.exe");
                p.WaitForInputIdle();
                if (!p.HasExited)
                    Find.SwitchEng(p.MainWindowHandle);
            };

            var d4 = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.D4, this);
            d4.HotKeyPressed += (s) =>
            {
                Process.Start(@"calc");
            };
            var crt = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.E, this);
            crt.HotKeyPressed += (s) =>
            {
                //Find.SwitchEng(new WindowInteropHelper(this).Handle);
                Process[] ps = Process.GetProcessesByName("SecureCRT");
                if (ps.Length > 0)
                {
                    Find.FocusProcess(ps[0]);
                    Find.SwitchEng(ps[0].MainWindowHandle);
                }
                else
                {
                    var p = Process.Start(@"C:\Program Files\VanDyke Software\SecureCRT\SecureCRT.exe");
                    //await Task.Delay(100);
                    p.WaitForInputIdle();
                    Find.bringToFront(p.MainWindowHandle);
                    //C:\Windows\System32\cmd.exe
                    //await Task.Delay(400);
                    //Find.SwitchEng(p.Handle);
                    //p.StartInfo.FileName = "cmd.exe";
                    //p.StartInfo.WorkingDirectory = @"c:\Users\crazy\Desktop\gits\";
                    //p.StartInfo.UseShellExecute = true;
                    //p.Start();
                }




            };

            var cmd = new HotKey(ModifierKeys.Alt, System.Windows.Forms.Keys.Oem3, this);
            cmd.HotKeyPressed += async (s) =>
            {
                //Find.SwitchEng(new WindowInteropHelper(this).Handle);
                Process[] ps = Process.GetProcessesByName("cmd");
                if (ps.Length > 0)
                {
                    Find.FocusProcess(ps[0]);
                }
                else
                {
                    var p = Process.Start("cmd", @"/k ""cd /d " + @"c:\Users\crazy\Desktop\gits\" + @"""");
                    await Task.Delay(100);
                    Find.bringToFront(@"C:\Windows\System32\cmd.exe");
                    //C:\Windows\System32\cmd.exe
                    //await Task.Delay(400);
                    //Find.SwitchEng(p.Handle);
                    //p.StartInfo.FileName = "cmd.exe";
                    //p.StartInfo.WorkingDirectory = @"c:\Users\crazy\Desktop\gits\";
                    //p.StartInfo.UseShellExecute = true;
                    //p.Start();
                }

                Find.bringToFront(@"C:\Windows\System32\cmd.exe");


            };
            tb.Focus();

        }
        private void MainWindowKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
            case Key.Escape:

                WindowState = WindowState.Minimized;
                //ShowInTaskbar = false;
                //Hide();
                break;
            case Key.Enter:
                string keyword = tb.Text;

                //fix multiple launch issue
                if (!sw.IsRunning)
                {
                    sw.Start();
                }
                else
                {
                    sw.Stop();
                    if (sw.Elapsed.TotalSeconds < 1.0) return;
                    else sw.Restart();
                }


                if (keyword.Length > 1)
                    switch (keyword.Substring(0, 2))
                    {
                    case "g ":
                        //SaveChange(1, 1);
                        r4.IsChecked = true;
                        keyword = keyword.Substring(2);
                        break;
                    case "b ":
                        //SaveChange(1, 2);
                        r5.IsChecked = true;
                        keyword = keyword.Substring(2);
                        break;
                    case "w ":
                        //SaveChange(1, 3);
                        r6.IsChecked = true;
                        keyword = keyword.Substring(2);
                        break;
                    default:
                        break;
                    }
                tb.SelectAll();
                Execute(currentSearch, keyword);
                if (!keyword.StartsWith("http"))
                    WindowState = WindowState.Minimized;
                break;
            case Key.F1:
                r4.IsChecked = true;
                voiceHint();
                //currentBrowser = 1;
                break;
            case Key.F2:
                r5.IsChecked = true;
                voiceHint();
                //currentBrowser = 2;
                break;
            case Key.F3:
                r6.IsChecked = true;
                voiceHint();
                //currentBrowser = 3;
                break;
            case Key.F4:
                //if ((System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Shift) != 0)
                //    Close();
                r7.IsChecked = true;
                voiceHint();
                break;
            case Key.W:
                if ((System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Control) != 0)
                    Close();
                break;
                //case Key.F5:
                //    r1.IsChecked = true;
                //    currentBrowser = 1;
                //    break;
                //case Key.F6:
                //    r2.IsChecked = true;
                //    currentBrowser = 2;
                //    break;
                //case Key.F7:
                //    r3.IsChecked = true;
                //    currentBrowser = 3;
                //    break;
            }
        }
        private async void Execute(int currentSearch, string keyword)
        {
            try
            {
                switch (keyword.Trim().ToLower())
                {
                case "vpn":
                    Process.Start("ncpa.cpl");
                    break;
                case "u":
                    //Process.Start("wuauclt.exe", "/showcheckforupdates");
                    Process.Start("control", "/name Microsoft.WindowsUpdate");
                    break;
                case "h":
                    Process.Start(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Hyper-V Management Tools\Hyper-V Manager.lnk");
                    break;
                case "!h":
                    cap.kill("mmc");
                    break;
                case "!vpn":
                    cap.kill("explorer");
                    break;
                case "q":
                    Process.Start(@"C:\Program Files (x86)\Tencent\QQ\Bin\QQ.exe");
                    break;
                case "c":
                    //if (Environment.MachineName != "HOME-PC")
                    ProcessStartInfo startInfo = new ProcessStartInfo("ConEmu64.exe");
                    startInfo.Verb = "runas";
                    startInfo.WorkingDirectory = @"C:\Users\Hongzhi\Desktop\";
                    Process.Start(startInfo);
                    //else
                    //    Process.Start(@"C:\Users\Hongzhi\SkyDrive\Scripts\elevatedCommand.bat");

                    break;
                case "r":
                    Process.Start(@"C:\Program Files (x86)\Remote Desktop Connection Manager\RDCMan.exe");
                    break;
                case "!r":
                    cap.kill("RDCMan");
                    break;
                case "o":
                    Process.Start(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\OneNote 2013.lnk");
                    break;
                case "t":
                    //    Process.Start(@"C:\Users\Hongzhi\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Total Commander\Total Commander.lnk");
                    //    break;
                    //case "task":
                    Process.Start("taskmgr.exe", "/6");
                    break;
                case "!t":
                    cap.minimize("TOTALCMD");
                    break;
                case "v":
                    Process.Start("devenv.exe");
                    break;
                case "f":
                    Process.Start(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Fiddler4.lnk");
                    break;
                case "ff":
                    Process.Start("firefox.exe");
                    break;
                case "w":
                    Process.Start(@"C:\Program Files (x86)\JetBrains\WebStorm 10.0.4\bin\WebStorm64.exe");
                    break;
                default:
                    //if (currentBrowser == 1)
                    //{

                    //以 'i ' 开头的搜索,查询ip地址所在地
                    var match = Regex.Match(keyword, @"\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b");
                    if (keyword.StartsWith("i") && match.Success)
                    {
                        var a = new WebClient();
                        var result = await a.DownloadStringTaskAsync("http://ip.taobao.com/service/getIpInfo.php?ip=" + HttpUtility.UrlEncode(match.Value));
                        var data = new JavaScriptSerializer().Deserialize<IpQuery>(Regex.Unescape(result));
                        var window = new result();
                        window.country.Text = data.data.country;
                        window.area.Text = data.data.area;
                        window.region.Text = data.data.region;
                        window.city.Text = data.data.city;
                        window.isp.Text = data.data.isp;
                        window.ip.Text = data.data.ip;
                        window.Show();
                        window.Activate();
                        //MessageBox.Show("地区:" + data.data.area + "\r\n" + "省份:" + data.data.region + "\r\n" + "city" + data.data.city + "\r\n" + "isp" + data.data.isp + "\r\n" + "ip" + data.data.ip + "\r\n");
                        //Clipboard.SetText(Regex.Unescape(result));
                        //MessageBox.Show(Regex.Unescape(result));
                    }
                    //以http开头的地址,调用短链接
                    else if (keyword.StartsWith("http://") || keyword.StartsWith("https://"))
                    {
                        var a = new WebClient();
                        var result = await a.DownloadStringTaskAsync("http://api.t.sina.com.cn/short_url/shorten.json?source=1681459862&url_long=" + HttpUtility.UrlEncode(keyword));
                        Clipboard.SetText(result.Substring(15, 19));
                        tb.Text = result.Substring(15, 19) + "  已复制到剪贴板";
                        await Task.Delay(1000);
                        WindowState = WindowState.Minimized;

                    }
                    //以sg 开头的搜索,调用搜狗号码通
                    else if (keyword.StartsWith("sg "))
                    {
                        Process.Start("http://haoma.sogou.com/rz/search_result.php?target_number=" + keyword.Substring(3));
                    }
                    else
                    {
                        //普通搜索,根据选择类型
                        if (currentSearch != 4)
                            Process.Start(searchTypes[currentSearch - 1] + HttpUtility.UrlEncode(keyword));

                        //有道api查单词
                        else
                        {
                            var apiResult = await LookUp(keyword) as Dictionary<string, object>;

                            var displayWindow = new LookupResult();
                            var displayString = "";

                            try
                            {
                                displayString += concatArray(ToStringArray(apiResult["translation"]));
                            }
                            catch (Exception)
                            {

                            };
                            displayString += "\r\n\r\n";

                            try
                            {
                                //displayString += apiResult.basic.explains.ToString();
                                displayString += concatArray(ToStringArray((apiResult["basic"] as Dictionary<string, object>)["explains"]));
                            }
                            catch (Exception)
                            {

                            }
                            displayWindow.textBlock.Text = displayString;
                            displayWindow.Show();
                            displayWindow.Activate();
                            YouDao.pronounce(keyword);
                        }
                    }


                    //else
                    //    Process.Start(browsers[currentBrowser - 1], searchTypes[currentSearch - 1] + HttpUtility.UrlEncode(keyword));
                    break;


                }
            }
            catch (Exception)
            {
                SystemSounds.Exclamation.Play();
                // MessageBox.Show(e.Message);
            }

        }


        private string[] ToStringArray(object arg)
        {
            var collection = arg as System.Collections.IEnumerable;
            if (collection != null)
            {
                return collection
                  .Cast<object>()
                  .Select(x => x.ToString())
                  .ToArray();
            }

            if (arg == null)
            {
                return new string[] { };
            }

            return new string[] { arg.ToString() };
        }
        private string concatArray(string[] arg)
        {
            var resutl = "";
            foreach (var item in arg)
            {
                resutl += (item + "\r\n");
            }
            return resutl;
        }
        private async Task<object> LookUp(string word)
        {
            try
            {
                var a = new WebClient();
                var result = await a.DownloadDataTaskAsync(new Uri("https://fanyi.youdao.com/fanyiapi.do?keyfrom=miniforce&key=812287220&type=data&doctype=json&version=1.1&q=" + HttpUtility.UrlEncode(word)));
                var b = Encoding.UTF8.GetString(result);
                return new JavaScriptSerializer().DeserializeObject(Regex.Unescape(b));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return "";
                //throw;
            }
        }
        private void onpaste(object sender, DataObjectPastingEventArgs e)
        {
            //if (((e.DataObject as DataObject)).GetText().IndexOf("\r\b") == -1)
            //{
            //    tb.Text = ((e.DataObject as DataObject)).GetText();
            //}
            //else
            //    tb.Text = removeCarriageReturn(((e.DataObject as DataObject)).GetText(), 0);
            ////afterPaste = true;
            //int index = ((e.DataObject as DataObject)).GetText().IndexOf("\r\n");
            //if (index > -1)
            //{
            //    string clipboard = ((e.DataObject as DataObject)).GetText().Replace("\r\n", " ");
            //    tb.Text = clipboard.Substring(index);
            //    //tb.Text = clipboard;
            //}
            ////await Task.Delay(1000);
            ////afterPaste = false;
        }
        private void RadioChecked(object sender, RoutedEventArgs e)
        {
            FrameworkElement feSource = e.Source as FrameworkElement;
            switch (feSource.Name)
            {
            //case "r1":
            //    SaveChange(0, 1);
            //    break;
            //case "r2":

            //    SaveChange(0, 2);
            //    break;
            //case "r3":

            //    SaveChange(0, 3);
            //    break;
            case "r4":
                tb.SetValue(TextBoxHelper.WatermarkProperty, "Google Search");
                SaveChange(1);
                break;
            case "r5":
                tb.SetValue(TextBoxHelper.WatermarkProperty, "YouDao Search");
                SaveChange(2);
                break;
            case "r6":
                tb.SetValue(TextBoxHelper.WatermarkProperty, "Wikipedia Search");
                SaveChange(3);
                break;
            case "r7":
                tb.SetValue(TextBoxHelper.WatermarkProperty, "有道 api");
                SaveChange(4);
                break;
            default:
                break;
            }
        }
        //value : 1 google search
        //2 youdao search
        //3 wikipedia search
        private void SaveChange(int value)
        {
            //if (type == 0)
            //{
            //    Settings.Default.CurrentBrowser = value;
            //    Settings.Default.Save();
            //    currentBrowser = value;
            //}
            //else
            //{
            //Settings.Default.CurrentSearch = value;
            //Settings.Default.Save();
            currentSearch = value;
            //}
        }
        public void globalHKPressed(HotKey obj)
        {
            InputLanguageManager.SetInputLanguage(tb, new CultureInfo("en-US"));

            //WindowState = WindowState.Normal;

            Show();
            WindowState = WindowState.Normal;
            //Activate();
            if (Clipboard.GetText(TextDataFormat.Text).StartsWith("http://t.cn"))
            {
                tb.Text = "";
                voiceHint();
                //InputMethod.SetPreferredImeState(tb, InputMethodState.Off);
                //if (WindowState == WindowState.Minimized)

                //ShowInTaskbar = false;
                //else

            }
            else
            {

                //ShowInTaskbar = false;
                //else
                //Activate();
                tb.Text = removeCarriageReturn(Clipboard.GetText(TextDataFormat.Text), 1);
                tb.SelectAll();
                voiceHint();
                //if (Clipboard.GetText(TextDataFormat.Text).StartsWith("http://") || Clipboard.GetText(TextDataFormat.Text).StartsWith("https://"))
                //{
                //}
                //else
                //{
                //    tb.Text = "";
                //}

                //var a = new WebClient();
                //var result = await a.DownloadStringTaskAsync("http://api.t.sina.com.cn/short_url/shorten.json?source=1681459862&url_long=" + Uri.EscapeUriString(tb.Text));
                //Clipboard.SetText(result.Substring(15, 19));
                //tb.Text = result.Substring(15, 19) + "  已复制到剪贴板！";
                //await Task.Delay(1000);
                //WindowState = WindowState.Minimized;

            }
            //var text = Clipboard.GetText(TextDataFormat.Text);
            //if (text.Length < 30) tb.Text = text.Replace("\r\n", "").Trim();
            ////tb.Text = Clipboard.GetText(TextDataFormat.Text);
            //tb.SelectAll();
            //InputMethod.SetPreferredImeState(tb, InputMethodState.Off);

        }
        private void voiceHint()
        {
            Stream sound = null;
            if (r4.IsChecked.Value)
                sound = Properties.Resources.google;
            else if (r5.IsChecked.Value)
                sound = Properties.Resources.dict;
            else if (r7.IsChecked.Value)
                sound = Properties.Resources.trans;
            else
                sound = Properties.Resources.wiki;

            SoundPlayer player = new SoundPlayer(sound);
            player.Play();
        }
        private string removeCarriageReturn(string input, int type)
        {
            int index = input.IndexOf("\r\n");
            if (index > -1)
            {
                string clipboard = input.Replace("\r\n", " ");
                input = type == 0 ? clipboard.Substring(index) : clipboard;
                //tb.Text = clipboard;
            }
            return input;

        }

        private bool IsPropertyExist(dynamic settings, string name)
        {
            return settings.GetType().GetProperty(name) != null;
        }

        //private void minimize(object sender, RoutedEventArgs e)
        //{
        //    WindowState = WindowState.Minimized;
        //    //Hide();
        //}

        //private void close(object sender, RoutedEventArgs e)
        //{
        //    Close();
        //}







    }
}
