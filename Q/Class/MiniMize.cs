﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;

namespace Q
{
    public static class MinimizeToTray
    {
        /// <summary>
        /// Enables "minimize to tray" behavior for the specified Window.
        /// </summary>
        /// <param name="window">Window to enable the behavior for.</param>
        public static void Enable(MainWindow window)
        {
            // No need to track this instance; its event handlers will keep it alive
            new MinimizeToTrayInstance(window);
        }

        /// <summary>
        /// Class implementing "minimize to tray" functionality for a Window instance.
        /// </summary>
        private class MinimizeToTrayInstance
        {
            private MainWindow _window;
            private NotifyIcon _notifyIcon;
            //private bool _balloonShown;

            /// <summary>
            /// Initializes a new instance of the MinimizeToTrayInstance class.
            /// </summary>
            /// <param name="window">Window instance to attach to.</param>
            public MinimizeToTrayInstance(MainWindow window)
            {
                Debug.Assert(window != null, "window parameter is null.");
                _window = window;
                _window.StateChanged += new EventHandler(HandleStateChanged);
            }

            /// <summary>
            /// Handles the Window's StateChanged event.
            /// </summary>
            /// <param name="sender">Event source.</param>
            /// <param name="e">Event arguments.</param>
            private void HandleStateChanged(object sender, EventArgs e)
            {
                var minimized = (_window.WindowState == WindowState.Minimized);
                if (minimized) _window.Hide();
                //else _window.Show();
                if (_notifyIcon == null)
                {
                    // Initialize NotifyIcon instance "on demand"
                    _notifyIcon = new NotifyIcon();
                    _notifyIcon.Icon = Icon.ExtractAssociatedIcon(Assembly.GetEntryAssembly().Location);
                    _notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(HandleNotifyIconOrBalloonClicked);
                    //_notifyIcon.BalloonTipClicked += new EventHandler(HandleNotifyIconOrBalloonClicked);
                    _notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] { new MenuItem("退出", onClick) });
                }
                // Update copy of Window Title in case it has changed
                _notifyIcon.Text = _window.Title;

                // Show/hide Window and NotifyIcon
                _window.ShowInTaskbar = false;
                _notifyIcon.Visible = minimized;
                
                //if (minimized && !_balloonShown)
                //{
                //    // If this is the first time minimizing to the tray, show the user what happened
                //    _notifyIcon.ShowBalloonTip(1000, null, _window.Title, ToolTipIcon.None);
                //    _balloonShown = true;
                //}
            }

            private void onClick(object sender, EventArgs e)
            {
                _notifyIcon.Visible = false;
                //_window.globalHK.Dispose();
                _window.Close();
            }

            /// <summary>
            /// Handles a click on the notify icon or its balloon.
            /// </summary>
            /// <param name="sender">Event source.</param>
            /// <param name="e">Event arguments.</param>
            private void HandleNotifyIconOrBalloonClicked(object sender, System.Windows.Forms.MouseEventArgs e)
            {
                // Restore the Window
                if (e.Button == MouseButtons.Left)
                {
                    _window.Show();
                    _window.WindowState = WindowState.Normal;
                    _window.globalHKPressed(_window.globalHK);
                }
            }
        }
    }
}
