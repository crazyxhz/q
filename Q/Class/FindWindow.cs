﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Q.Class
{
    class Find
    {
        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);

        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr LoadKeyboardLayout(string pwszKLID, uint Flags);
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool BringWindowToTop(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(HandleRef hWnd, int nCmdShow);


        public static int bringToFront(object titles)
        {
            if (titles.GetType().Name == "String")
            {
                var title = (string)titles;
                IntPtr handle = FindWindow(null, title);

                // Verify that Calculator is a running process.
                if (handle == IntPtr.Zero)
                {
                    return -1;
                }

                // Make Calculator the foreground application
                //InputLanguageManager.SetInputLanguage(tb, new CultureInfo("en-US"));
                //ShowWindow(handle, 5);  // Make the window visible if it was hidden


                //0xF030 max  0xF120 restore
                PostMessage(handle, 0x0112, 0xF030, 0);
                SetForegroundWindow(handle);
                //ShowWindow(handle, 9);
                //BringWindowToTop(handle);
                //ShowWindow(handle, 3);  // Next, restore it if it was minimized
                //PostMessage(handle, 0x0050, 0, LoadKeyboardLayout("00000409", 0x00000001).ToInt32());
                SwitchEng(handle);
                return 1;
            }
            else
            {
                var handle = (IntPtr)titles;
                PostMessage(handle, 0x0112, 0xF030, 0);
                SetForegroundWindow(handle);
                //ShowWindow(handle, 9);
                //BringWindowToTop(handle);
                //ShowWindow(handle, 3);  // Next, restore it if it was minimized
                //PostMessage(handle, 0x0050, 0, LoadKeyboardLayout("00000409", 0x00000001).ToInt32());
                SwitchEng(handle);
                return 1;
            }
            // Get a handle to the Calculator application.

        }

        public static void SwitchEng(IntPtr handle)
        {
            PostMessage(handle, 0x0050, 0, LoadKeyboardLayout("00000409", 0x00000001).ToInt32());
        }

        public static void FocusProcess(Process procName)
        {

            IntPtr hWnd = IntPtr.Zero;
            hWnd = procName.MainWindowHandle;
            ShowWindowAsync(new HandleRef(null, hWnd), 3);
            SetForegroundWindow(procName.MainWindowHandle);
            //procName.WaitForInputIdle();
            //SwitchEng(procName.MainWindowHandle);

        }

        public static WINDOWPLACEMENT GetPlacement(IntPtr hwnd)
        {
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            placement.length = Marshal.SizeOf(placement);
            GetWindowPlacement(hwnd, ref placement);
            return placement;
        }

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetWindowPlacement(
            IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public ShowWindowCommands showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        internal enum ShowWindowCommands : int
        {
            Hide = 0,
            Normal = 1,
            Minimized = 2,
            Maximized = 3,
        }

        
    }
}
