﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Q.Class
{
    class CloseActiveProcess
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        public string GetActiveProcessFileName()
        {
            IntPtr hwnd = GetForegroundWindow();
            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            Process p = Process.GetProcessById((int)pid);
            return p.MainModule.FileName;
        }
        public void kill(string name)
        {
            try
            {
                Process[] proc = Process.GetProcessesByName(name);
                proc[0].Kill();
            }
            catch
            {

            }
        }
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        public void minimize(string name)
        {
            try
            {
                Process[] proc = Process.GetProcessesByName(name);
                ShowWindow(proc[0].Handle, 7);
            }
            catch
            {

            }
        }
    }
}
