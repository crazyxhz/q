﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NAudio;
using NAudio.Wave;
using System.IO;
using System.Windows;

namespace Q.Class
{
    class YouDao
    {
        static public void pronounce(string word)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(async delegate {
                await loadmp3(word);
            }, null);
           

        }
        static public async Task loadmp3(string word)
        {
            try
            {
                var a = new WebClient();
                var result = await a.DownloadDataTaskAsync(new Uri("http://dict.youdao.com/dictvoice?audio=" + HttpUtility.UrlEncode(word)+ "&type=2"));
                //var b = Encoding.UTF8.GetString(result);

                // var frame = Mp3Frame.LoadFromStream(new MemoryStream(result));


                using (WaveStream blockAlignedStream
                    = new BlockAlignReductionStream(WaveFormatConversionStream.CreatePcmStream(new Mp3FileReader(new MemoryStream(result)))))
                {
                    using (WaveOut waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback()))
                    {
                        waveOut.Init(blockAlignedStream);
                        waveOut.Play();
                        while (waveOut.PlaybackState == PlaybackState.Playing)
                        {
                            System.Threading.Thread.Sleep(100);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
