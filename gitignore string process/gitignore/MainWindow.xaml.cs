﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gitignore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            //casscade ignore single file in deep directory
            var input = tb.Text;//"c:\Program Files\ArcGIS\Portal\webapps\arcgis#home\js\esri\arcgisonline\css\esri.css"
            input = input.Replace(@"c:\Program Files\ArcGIS\Portal\webapps\arcgis#home\", "");
            input = input.Replace("\\", "/");
            var origin = String.Copy(input);
            tb.Text = "";
            bool c = true;
            int b = 0;
            var seg = "";
            while (input.IndexOf("/") != -1)
            {
                b += input.IndexOf("/");
                seg = origin.Substring(0, b);
                input = origin.Substring(++b);
               
                    tb.Text += ("!" + seg + Environment.NewLine);
                    tb.Text += (seg + "/*" + Environment.NewLine);
                
               
            }
            //tb.Text += (seg + "/*" + Environment.NewLine);
            tb.Text += ("!" + seg + "/" + input);
            Clipboard.SetText(tb.Text.Replace("\r\n", "\n"));

        }
    }
}
